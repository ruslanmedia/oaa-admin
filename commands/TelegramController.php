<?php
/**
 * Created by PhpStorm.
 * User: ruslanmedia
 * Date: 6/18/18
 * Time: 12:19
 */

namespace app\commands;

use Yii;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Telegram;
use yii\console\ExitCode;
use yii\console\Controller;

class TelegramController extends Controller
{
    public function actionIndex()
    {
        return ExitCode::OK;
    }

    public function actionGetUpdates()
    {
        $commands_paths = [
            Yii::getAlias('@app/tcommands/'),
        ];

        $mysql_credentials = [
            'host'     => '127.0.0.1',
            'user'     => 'oaa',
            'password' => 'vguku9P090zQdDRL',
            'database' => 'oaa_mp3',
        ];

        try {
            // Create Telegram API object
            $telegram = new Telegram(Yii::$app->params['bot.api_key'], Yii::$app->params['bot.name']);

            $telegram->enableAdmin(200268315);

            // Enable MySQL
            $telegram->enableMySql($mysql_credentials);

            $telegram->addCommandsPaths($commands_paths);

            // Handle telegram getUpdates request
            $telegram->handleGetUpdates();
            return ExitCode::OK;
        } catch (TelegramException $e) {
            // log telegram errors
             echo $e->getMessage();
            return ExitCode::OK;
        }
    }
}