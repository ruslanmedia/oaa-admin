<?php
/**
 * Created by PhpStorm.
 * User: ruslanmedia
 * Date: 6/14/18
 * Time: 15:58
 */

namespace app\models;


use app\helpers\Mp3File;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class Songs extends ActiveRecord
{

    const UPLOADED_BY_SITE = 0;
    const UPLOADED_BY_TELEGRAM = 1;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const TELEGRAM_NOT_SENT = 0;
    const TELEGRAM_SENT = 1;

    public $file;
    public $file_content = null;

    public static function tableName()
    {
        return '{{%songs}}';
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author' => 'Ijrochi',
            'title' => 'Qo‘shiq nomi',
            'file_name' => 'Fayl nomi',
            'add_time' => 'Joylash vaqti',
            'uploaded_by' => 'Yuklangan',
            'uploaded_by_text' => 'Yuklangan',
            'uploaded_by_id' => 'Yuklagan shaxs ID',
            'telegram_sent' => 'Telegram kanalga joylash',
            'telegram_sent_text' => 'Telegram kanalga joylash',
            'status' => 'Holati',
            'status_text' => 'Holati',
            'file' => 'Faylni tanlang',
            'telegram_extra_message' => 'Telegramdagi kanalga qo‘shimcha xabar',
            'fileUrl' => 'Fayl manzilini kiriting',
        ];
    }

    public function rules()
    {
        return [
            [['author', 'title', 'time', 'uploaded_by', 'uploaded_by_id', 'telegram_sent', 'status'], 'required'],
            [['author', 'title', 'time', 'telegram_extra_message', 'hash_md5'], 'string', 'max' => 255],
            ['time', 'datetime', 'format' => 'php:Y-m-d H:i:s'],
            [['uploaded_by', 'uploaded_by_id', 'telegram_sent', 'status'], 'integer'],
            ['file', 'file', 'skipOnEmpty' => true, 'extensions' => 'mp3'],
            ['fileUrl', 'url'],
        ];
    }

    public static function listStatuses()
    {
        return [
            self::STATUS_ACTIVE => 'Aktiv',
            self::STATUS_INACTIVE => 'Aktiv emas',
        ];
    }

    public function getFileUrl()
    {
        $this->fileUrl = '';
    }

    public function setFileUrl($value)
    {
        $this->fileUrl = $value;
    }

    public function getTime()
    {
        return $this->time = isset($this->add_time) ? date('Y-m-d H:i:s', $this->add_time) : date('Y-m-d H:i:s');
    }

    public function setTime($value)
    {
        if (!empty($value)) {
            $this->add_time = strtotime($value);
        } else {
            $this->add_time = NULL;
        }
    }

    public function beforeSave($insert)
    {
        $fileFolder = $this->checkFolder(Yii::getAlias(Yii::$app->params['path.songs']));
        $tempfileFolder = $this->checkFolder(Yii::getAlias(Yii::$app->params['path.tempsongs']));

        if ($this->fileUrl) {
            $file = file_get_contents($this->fileUrl);
            $fileExtension = pathinfo(parse_url($this->fileUrl, PHP_URL_PATH), PATHINFO_EXTENSION);
            $this->hash_md5 = md5($file);
            $filename_file = $this->hash_md5 . ".{$fileExtension}";
            if (self::find()->where(['file_name' => $filename_file])->exists()) {
                return $this->addError('file', 'Bu fayl allaqachon joylangan');
            }
            file_put_contents($fileFolder . $filename_file, $file);
        } elseif ($file = UploadedFile::getInstance($this, 'file')) {
            $filePath = $file->tempName;
            $this->hash_md5 = md5_file($filePath);
            $filename_file = $this->hash_md5 . ".{$file->extension}";
            if (self::find()->where(['file_name' => $filename_file])->exists()) {
                return $this->addError('file', 'Bu fayl allaqachon joylangan');
            }
            $file->saveAs($fileFolder . $filename_file);
        } else {
            $file = $this->file_content;
            $this->hash_md5 = md5($file);
            $filename_file = $this->hash_md5 . ".mp3";
            if (self::find()->where(['file_name' => $filename_file])->exists()) {
                return $this->addError('file', 'Bu fayl allaqachon joylangan');
            }
            file_put_contents($fileFolder . $filename_file, $file);
        }

        if ($file) {
            $data = [
                "mp3_songname" => trim(str_replace('&#39;', '\'', $this->title)),
                "mp3_artist" => trim(str_replace('&#39;', '\'', $this->author)),
                "mp3_album" => "UZHITS.NET",
                "mp3_year" => date('Y'),
                "mp3_genre" => "UZHITS.NET",
                "mp3_comment" => "UZHITS.NET",
                "mp3_image" => [
                    "path" => Yii::getAlias('@app/web/logo.jpg'),
                    "type" => 'image/jpeg',
                    "name" => "logo.jpg"
                ]
            ];

            Mp3File::writeTags($fileFolder . $filename_file, $data);

            $this->file_name = $filename_file;


            if (Yii::$app->params['ftp.enabled'] == true) {
                $ftp = new \yii2mod\ftp\FtpClient();
                $ftp->connect(Yii::$app->params['ftp.host']);
                $ftp->login(Yii::$app->params['ftp.username'], Yii::$app->params['ftp.password']);
                $ftp->pasv(true);

                $handle = fopen($fileFolder . $filename_file, 'r');
                if ($ftp->fput(Yii::$app->params['ftp.path'] . $filename_file, $handle, FTP_BINARY)) {
                    rewind($handle);
                }
            }
        }
        unset($file);

        return parent::beforeSave($insert);
    }

    public function getUploaded_by_text()
    {
        $data = [
            self::UPLOADED_BY_SITE => 'Sayt orqali yuklangan',
            self::UPLOADED_BY_TELEGRAM => 'Telegram orqali yuklangan',
        ];

        return $data[$this->uploaded_by];
    }

    public function getTelegram_sent_text()
    {
        $data = [
            self::TELEGRAM_NOT_SENT => 'Joylanmagan',
            self::TELEGRAM_SENT => 'Kanalga yuborilgan',
        ];

        return $data[$this->telegram_sent];
    }

    public function getStatus_text()
    {
        $data = [
            self::STATUS_ACTIVE => 'Aktiv',
            self::STATUS_INACTIVE => 'Aktiv emas',
        ];

        return $data[$this->status];
    }

    public function beforeDelete()
    {
        $fileFolder = $this->checkFolder(Yii::getAlias(Yii::$app->params['path.songs']));

        @unlink($fileFolder . $this->file_name);

        return parent::beforeDelete();
    }

    private function checkFolder($path)
    {
        if (!file_exists($path)) {
            try {
                FileHelper::createDirectory($path, 0777);
                return $path;
            } catch (\yii\base\Exception $e) {
                Yii::error('Qo‘shiq papkasini yaratishda muammo yuzaga keldi');
            }
        } else {
            return $path;
        }

        return false;
    }
}