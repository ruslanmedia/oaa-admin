<?php
/**
 * Created by PhpStorm.
 * User: ruslanmedia
 * Date: 6/14/18
 * Time: 16:19
 */

use rmrevin\yii\fontawesome\FA;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = 'Qo‘shiqlar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pull-right">
    <?=Html::a('Yangi joylash', ['add'], ['class' => 'btn btn-success']) ?>
</div>
<h3 class="box-title"><?= $this->title ?></h3>

<?=GridView::widget([
    'dataProvider' => $dataProvider,
    'rowOptions' => function ($model, $key, $index, $grid) {
        if ($model->status == 0) {
            return ['class' => 'danger'];
        } elseif ($model->status == 1) {
            return ['class' => 'success'];
        } else {
            return ['class' => 'warning'];
        }
    },
    'tableOptions' => ['class' => 'table table-bordered table-hover'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'author',
        'title',
        'telegram_sent_text',
        'add_time:datetime',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{st} {view} {update} {delete}',
            'buttons' => [
                'st' => function ($url, $model, $key) {
                    if ($model->telegram_sent == \app\models\Songs::TELEGRAM_NOT_SENT) {
                        return Html::a(FA::icon('telegram'), ['send-telegram', 'id' => $model->id]);
                    }
                }
            ],
        ]
    ],
]) ?>