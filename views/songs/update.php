<?php
/**
 * Created by PhpStorm.
 * User: ruslanmedia
 * Date: 6/14/18
 * Time: 17:42
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $model \app\models\Songs */

$this->title = 'O‘zgartirish';
$this->params['breadcrumbs'][] = ['label' => 'Qo‘shiqlar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin(); ?>
<?= $form->errorSummary($model) ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'author') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'title') ?>
        </div>
    </div>
<?= $form->field($model, 'telegram_extra_message') ?>
<?= $form->field($model, 'time')->textInput() ?>
<?= $form->field($model, 'status')->dropDownList($model->listStatuses()) ?>
<?= Html::submitButton('Saqlash', ['class' => 'btn btn-primary']) ?>

<?php ActiveForm::end(); ?>