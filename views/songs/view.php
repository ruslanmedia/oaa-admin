<?php
/**
 * Created by PhpStorm.
 * User: ruslanmedia
 * Date: 6/14/18
 * Time: 17:31
 */

use yii\widgets\DetailView;

/* @var $model \app\models\Songs */

$this->title = "{$model->author} - {$model->title}";
$this->params['breadcrumbs'][] = ['label' => 'Qo‘shiqlar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'author',
        'title',
        'file_name',
        'add_time:datetime',
        'uploaded_by_text',
        'uploaded_by_id',
        'telegram_sent_text',
        'telegram_extra_message',
        'status_text'
    ],
]) ?>