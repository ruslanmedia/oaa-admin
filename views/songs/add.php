<?php
/**
 * Created by PhpStorm.
 * User: ruslanmedia
 * Date: 6/14/18
 * Time: 16:22
 */

/* @var $model \app\models\Songs */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Yangi joylash';

$this->params['breadcrumbs'][] = ['label' => 'Qo‘shiqlar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin(); ?>
<?= $form->errorSummary($model) ?>
<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'author') ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'title') ?>
    </div>
</div>
    <div>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#byFile" aria-controls="byFile" role="tab" data-toggle="tab">Fayl orqali yuklash</a></li>
            <li role="presentation"><a href="#byUrl" aria-controls="byUrl" role="tab" data-toggle="tab">URL orqali yuklash</a></li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="byFile">
                <?= $form->field($model, 'file')->fileInput() ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="byUrl">
                <?= $form->field($model, 'fileUrl') ?>
            </div>
        </div>

    </div>
<?= $form->field($model, 'telegram_extra_message') ?>
<?= $form->field($model, 'time')->textInput() ?>
<?= $form->field($model, 'status')->dropDownList($model->listStatuses()) ?>
<?= Html::submitButton('Saqlash', ['class' => 'btn btn-primary']) ?>

<?php ActiveForm::end(); ?>