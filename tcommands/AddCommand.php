<?php
/**
 * Created by PhpStorm.
 * User: ruslanmedia
 * Date: 6/18/18
 * Time: 13:07
 */

namespace Longman\TelegramBot\Commands\AdminCommands;


use app\models\Songs;
use Longman\TelegramBot\Commands\AdminCommand;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Request;
use Yii;

class AddCommand extends AdminCommand
{
    protected $name = 'add';
    protected $description = 'MP3 Qo‘shish';
    protected $usage = '/add';
    protected $version = '1.0';
    protected $need_mysql = true;
    protected $private_only = true;
    protected $conversation;

    public function execute()
    {
        $message = $this->getMessage();
        $chat    = $message->getChat();
        $user    = $message->getFrom();
        $text    = trim($message->getText(true));
        $chat_id = $chat->getId();
        $user_id = $user->getId();
        //Preparing Response
        $data = [
            'chat_id' => $chat_id,
        ];
        if ($chat->isGroupChat() || $chat->isSuperGroup()) {
            //reply to message id is applied by default
            //Force reply is applied by default so it can work with privacy on
            $data['reply_markup'] = Keyboard::forceReply(['selective' => true]);
        }
        //Conversation start
        $this->conversation = new Conversation($user_id, $chat_id, $this->getName());
        $notes = &$this->conversation->notes;
        !is_array($notes) && $notes = [];
        //cache data from the tracking session if any
        $state = 0;
        if (isset($notes['state'])) {
            $state = $notes['state'];
        }
        $result = Request::emptyResponse();
        //State machine
        //Entrypoint of the machine state if given by the track
        //Every time a step is achieved the track is updated
        switch ($state) {
            case 0:
                if ($text === '') {
                    $notes['state'] = 0;
                    $this->conversation->update();
                    $data['text']         = 'Ijrochini kiriting:';
                    $data['reply_markup'] = Keyboard::remove(['selective' => true]);
                    $result = Request::sendMessage($data);
                    break;
                }
                $notes['author'] = $text;
                $text          = '';
            // no break
            case 1:
                if ($text === '') {
                    $notes['state'] = 1;
                    $this->conversation->update();
                    $data['text'] = 'Qo‘shiq nomini kiriting:';
                    $result = Request::sendMessage($data);
                    break;
                }
                $notes['title'] = $text;
                $text             = '';
            // no break
            case 2:
                if ($text === '') {
                    $notes['state'] = 2;
                    $this->conversation->update();
                    $data['text'] = 'Joylanish sanasini kiriting: yyyy-mm-dd hh:ii:ss';
                    $result = Request::sendMessage($data);
                    break;
                }
                $notes['add_time'] = $text;
                $text             = '';
            // no break
            case 3:
                if ($text === '' || !in_array($text, ['Faol', 'Faol emas'], true)) {
                    $notes['state'] = 3;
                    $this->conversation->update();
                    $data['reply_markup'] = (new Keyboard(['Faol', 'Faol emas']))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(true);
                    $data['text'] = 'Holatini tanlang:';
                    if ($text !== '') {
                        $data['text'] = 'Iltimos, quyidagi tugmalardan birini tanlang:';
                    }
                    $result = Request::sendMessage($data);
                    break;
                }
                $notes['status'] = $text;
                $text             = '';
            // no break
            case 4:
                if ($text === '') {
                    $notes['state'] = 4;
                    $this->conversation->update();
                    $data['text'] = 'Kanalga yuborishda qo‘shimcha xabarni yozing:';
                    $result = Request::sendMessage($data);
                    break;
                }
                $notes['extra_message'] = $text;
                $text             = '';
            // no break
            case 5:
                if ($message->getAudio() === null) {
                    $notes['state'] = 5;
                    $this->conversation->update();
                    $data['text'] = 'Faylni yuboring:';
                    $result = Request::sendMessage($data);
                    break;
                }
                $audio             = $message->getAudio();
                $notes['audio_id'] = $audio->getFileId();
            // no break
            case 6:
                $this->conversation->update();
                unset($notes['state']);

                $model = new Songs();
                $model->author = $notes['author'];
                $model->title = $notes['title'];
                $model->file_content = Request::getFile(['file_id' => $notes['audio_id']])->getResult();
                $model->uploaded_by = Songs::UPLOADED_BY_TELEGRAM;
                $model->uploaded_by_id = $user_id;
                $model->time = $notes['add_time'];
                $model->telegram_sent = Songs::TELEGRAM_NOT_SENT;
                switch ($notes['status']) {
                    case 'Faol':
                        $model->status = Songs::STATUS_ACTIVE;
                    break;
                    case 'Faol emas':
                        $model->status = Songs::STATUS_INACTIVE;
                    break;
                }
                $model->telegram_extra_message = ($notes['extra_message'] == '.') ? null : $notes['extra_message'];

                if($model->save()) {
                    $data['text'] = 'Saqlandi!';
                    $result = Request::sendMessage($data);
                } else {
                    $data['text'] = json_encode($model->getErrors());
                    $result = Request::sendMessage($data);
                }
                $this->conversation->stop();
                break;
        }
        return $result;
    }
}