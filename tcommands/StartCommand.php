<?php
/**
 * Created by PhpStorm.
 * User: ruslanmedia
 * Date: 6/18/18
 * Time: 12:36
 */

namespace Longman\TelegramBot\Commands\SystemCommands;


use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Request;

class StartCommand extends SystemCommand
{
    protected $name = 'start';
    protected $description = 'start uchun';
    protected $usage = '/start';
    protected $version = '1.0.0';
    protected $private_only = false;

    public function execute()
    {
        $message = $this->getMessage();
        $chat_id = $message->getChat()->getId();
        $text    = 'Hi there!' . PHP_EOL . 'Type /help to see all commands!';
        $data = [
            'chat_id' => $chat_id,
            'text'    => $text,
        ];
        return Request::sendMessage($data);
    }
}