<?php
/**
 * Created by PhpStorm.
 * User: ruslanmedia
 * Date: 6/10/18
 * Time: 18:43
 */

namespace app\helpers;


use getID3;
use getid3_writetags;

class Mp3File
{
    public static function writeTags($mp3_file,$data=""){
        if(!is_file($mp3_file))return false;

        $mp3_tagformat = 'UTF-8';
        //require_once(Yii::getAlias('@vendor').'/james-heinrich/getid3/getid3/getid3.php');

        $mp3_handler = new getID3;
        $mp3_handler->setOption(array('encoding'=>$mp3_tagformat));

        //require_once(Yii::getAlias('@vendor').'/james-heinrich/getid3/getid3/write.php');
        $mp3_writter = new getid3_writetags;
        $mp3_writter->filename       = $mp3_file;
        $mp3_writter->tagformats     = array('id3v1', 'id3v2.3');
        $mp3_writter->overwrite_tags = true;
        $mp3_writter->tag_encoding   = $mp3_tagformat;
        $mp3_writter->remove_other_tags = false;

        $mp3_data['title'][]   = $data['mp3_songname'];
        $mp3_data['artist'][]  = $data['mp3_artist'];
        $mp3_data['album'][]   = $data['mp3_album'];
        $mp3_data['year'][]    = $data['mp3_year'];
        $mp3_data['genre'][]   = $data['mp3_genre'];
        $mp3_data['comment'][] = $data['mp3_comment'];

        $mp3_data['attached_picture'][0]['data'] = file_get_contents($data['mp3_image']['path']);
        $mp3_data['attached_picture'][0]['picturetypeid'] = $data['mp3_image']['type'];
        $mp3_data['attached_picture'][0]['description'] = $data['mp3_image']['name'];
        $mp3_data['attached_picture'][0]['mime'] = $data['mp3_image']['type'];

        $mp3_writter->tag_data = $mp3_data;
        $mp3_writter->WriteTags();
    return true;
  }
}