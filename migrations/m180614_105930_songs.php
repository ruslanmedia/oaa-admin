<?php

use yii\db\Migration;

/**
 * Class m180614_105930_songs
 */
class m180614_105930_songs extends Migration
{
    public function up()
    {
        $this->createTable(\app\models\Songs::tableName(), [
            'id' => $this->primaryKey(),
            'author' => $this->string(255)->notNull(),
            'title' => $this->string(255)->notNull(),
            'file_name' => $this->string(255)->notNull(),
            'add_time' => $this->integer(11)->notNull(),
            'uploaded_by' => $this->tinyInteger(1)->notNull(),
            'uploaded_by_id' => $this->integer(11)->notNull(),
            'telegram_sent' => $this->tinyInteger(1)->defaultValue(0)->notNull(),
            'status' => $this->tinyInteger(1)->defaultValue(0)->notNull(),
        ]);
    }

    public function down()
    {
        $this->delete(\app\models\Songs::tableName());
    }

}
