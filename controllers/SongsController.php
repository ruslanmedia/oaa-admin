<?php
/**
 * Created by PhpStorm.
 * User: ruslanmedia
 * Date: 6/14/18
 * Time: 15:57
 */

namespace app\controllers;


use app\models\Songs;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;
use Yii;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class SongsController extends Controller
{
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Songs::find(),
            'sort' => [
                'defaultOrder' => ['add_time' => SORT_DESC],
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionAdd()
    {
        $model = new Songs();
        $model->uploaded_by = Songs::UPLOADED_BY_SITE;
        $model->uploaded_by_id = Yii::$app->user->id;
        $model->telegram_sent = Songs::TELEGRAM_NOT_SENT;
        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            return $this->redirect(['index']);
        } else {
            return $this->render('add', [
                'model' => $model
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = Songs::find()->where(['id' => $id])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionSendTelegram($id)
    {
        $song = $this->findModel($id);

        if ($song->telegram_sent == Songs::TELEGRAM_NOT_SENT) {
            $telegram = new Telegram(Yii::$app->params['bot.api_key'], Yii::$app->params['bot.name']);

            $data['chat_id'] = Yii::$app->params['bot.channel_name'];
            $data['performer'] = $song->author;
            $data['title'] = $song->title;
            $data['parse_mode'] = "Markdown";

            $filePath = Yii::getAlias(Yii::$app->params['path.songs']) . $song->file_name;
            $fileSize = number_format(filesize($filePath) / 1048576, 2);

            $data['audio'] = Request::encodeFile($filePath);

            $data['caption'] = "🌟 {$song->author}\n🔊 {$song->title} #{$fileSize} mb\n✔ website: https://uzhits.net/\n👉 [Kanalga obuna bo‘lish](https://telegram.me/joinchat/AAAAAEDEJWjG-nh3s--txQ)👈\n{$song->telegram_extra_message}";
            $result = Request::sendAudio($data);
//            $result = json_decode($result);
            $song->telegram_sent = Songs::TELEGRAM_SENT;
            $song->telegram_sent_id = $result->result->message_id;
            $song->save();
            $this->redirect(['songs/index']);
        } else {
            throw new Exception('Ushbu qo‘shiq allaqachon Telegramdagi kanalga joylangan');
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Songs::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}