<?php
/**
 * Created by PhpStorm.
 * User: ruslanmedia
 * Date: 6/18/18
 * Time: 11:27
 */

namespace app\controllers;


use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Telegram;
use Yii;
use yii\web\Controller;

class TelegramController extends Controller
{
    public function actionHook()
    {

    }

    public function actionHookSet()
    {
        $hook_url = 'https://mp3.oaa.uz/telegram/hook';

        try {
            $telegram = new Telegram(Yii::$app->params['bot.api_key'], Yii::$app->params['bot.name']);

            $result = $telegram->setWebhook($hook_url);
            if ($result->isOk()) {
                echo $result->getDescription();
            }
        } catch (TelegramException $e) {
            echo $e->getMessage();
        }
    }

    public function actionHookUnset()
    {
        try {
            $telegram = new Telegram(Yii::$app->params['bot.api_key'], Yii::$app->params['bot.name']);

            $result = $telegram->setWebhook('');
            if ($result->isOk()) {
                echo $result->getDescription();
            }
        } catch (TelegramException $e) {
            echo $e->getMessage();
        }
    }

    public function actionGetUpdates()
    {
        $commands_paths = [
            Yii::getAlias('@app/tcommands/'),
        ];

        $mysql_credentials = [
            'host'     => 'localhost',
            'user'     => 'oaa',
            'password' => 'vguku9P090zQdDRL',
            'database' => 'oaa_mp3',
        ];

        try {
            // Create Telegram API object
            $telegram = new Telegram(Yii::$app->params['bot.api_key'], Yii::$app->params['bot.name']);

            $telegram->enableAdmin(200268315);

            // Enable MySQL
            $telegram->enableMySql($mysql_credentials);

            //$telegram->addCommandsPaths($commands_paths);

            // Handle telegram getUpdates request
            return $telegram->handleGetUpdates();
        } catch (TelegramException $e) {
            // log telegram errors
            return $e->getMessage();
        }
    }
}