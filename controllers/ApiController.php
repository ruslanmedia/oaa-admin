<?php
/**
 * Created by PhpStorm.
 * User: ruslanmedia
 * Date: 6/18/18
 * Time: 10:40
 */

namespace app\controllers;


use app\models\Songs;
use yii\rest\Controller;

class ApiController extends Controller
{
    public function actionSongs($limit=10) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $songs = Songs::find()->where(['status' => 1])->andWhere(['<', 'add_time', time()])->limit($limit)->orderBy(['add_time' => SORT_DESC]);

        foreach ($songs->all() as $song) {
            $allSongs[] = ['artist' => $song->author, 'title' => $song->title, 'add_time' => $song->add_time, 'md5' => $song->hash_md5];
        }

        return $allSongs;
    }
}